<?php

namespace Model;

class Service extends \Emagid\Core\Model {
    static $tablename = "public.service";

    public static $fields  =  [
    	'name'=>['required'=>true],
    	'description',
		'slogan',
    	'photo',
    	'sub_service' => ['type'=>'numeric'],
		'slug' => ['required'=>true,'unique'=>true],
		'meta_title', 
		'meta_keywords', 
		'meta_description',
		'display_order' => ['type'=>'numeric'],
		'featured_image',
		'description_landing'
    ];    

    static $relationships = [
		[
			'name'=>'provider_service',
			'class_name' => '\Model\Provider_Services',
			'local'=>'id',
			'remote'=>'service_id',
			'remote_related'=>'service_id',
			'relationship_type' => 'many'
		],
    ];    
}
























