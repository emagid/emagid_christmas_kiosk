<?php
namespace Model;

class Product_Map extends \Emagid\Core\Model {
    public static $tablename = "product_map";

    public static $fields = [
        'product_id',
        'product_type_id'
    ];


    /**
     * @param $id
     * @param $type
     * @return mixed
     * @throws \Exception
     */
    public function getProduct($id, $type)
    {
        if(!is_numeric($type)){
            $type = Product_Type::getTypeId($type);
        }

        $type = Product_Type::getType($type);
        $type = '\Model\\'.$type;
        return $type::getItem($id);
    }

    /**
     * @param $productId
     * @param $type
     * @return null|Object
     * @throws \Exception
     */
    public function getMap($productId, $type)
    {
        if(!is_numeric($type)){
            $type = Product_Type::getTypeId($type);
        }

        if($this->getProduct($productId, $type)){
            if($map = self::getItem(null, ['where' => "product_id = $productId and product_type_id = $type"])){
                return $map;
            } else {
                // create new map
                $map = new Product_Map();
                $map->product_id = $productId;
                $map->product_type_id = $type;
                $map->save();
                return $map;
            }
        }

        return null;
    }

    public function product()
    {
        return $this->getProduct($this->product_id, $this->product_type_id);
    }
}